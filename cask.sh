#!/usr/bin/env bash



# install brew cask
brew tap caskroom/cask

# gnuplot needs aquaterm
brew cask install aquaterm

# web browsers
brew cask install google-chrome
# brew cask install firefox

# Dropbox and GoogleDrive
brew cask install dropbox
#brew cask install google-drive
#brew cask install google-drive-file-stream
brew cask install google-backup-and-sync

# Virturalbox
# brew cask install virtualbox

# VLC media player, Gimp, Blender
brew cask install vlc
brew cask install audacity soundflower
brew cask install blender
brew cask install gimp
brew cask install xquartz
brew cask install inkscape

# text editors: Mou, macvim, textwrangler, atom, sublime
#brew cask install mou # not supporting Sierra yet on Oct 18, 2017
brew cask install textwrangler
brew cask install macvim
#brew cask install atom
#brew cask install sublime-text

# QQ --- QQmusic #qqmusic #qqinput
brew cask install qq
# use sogouinput input instead of qqinput
brew cask install sogouinput
brew cask install neteasemusic

# Steam
brew cask install steam

# avogadro
brew cask install avogadro

# pdf readers: Skim Adobe-Reader
brew cask install skim
brew cask install adobe-reader

# skype
brew cask install skype

# transmission torrent client
brew cask install transmission

# Mendeley and Calibre
brew cask install mendeley-desktop
#brew cask install calibre

# install mactex via Homebrew
brew cask install mactex

#brew cask install codeblocks

brew cask install eclipse-cpp

# doxygen 
brew cask install doxygen

