

# Add tab completion for many Bash commands
if [ -f $(brew --prefix)/etc/bash_completion ]; then
  source $(brew --prefix)/etc/bash_completion
fi

# Load definitions from files .paths, .exports, .aliases, .functions
for file in ~/.{paths,exports,aliases,functions}; do
  [ -r "$file" ] && [ -f "$file" ] && source "$file";
done;
unset file;

# Enable z program
source $(brew --prefix)/etc/profile.d/z.sh


