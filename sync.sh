#!/usr/bin/env bash

cd "$(dirname "${BASH_SOURCE}")";

git pull origin master;

#function doIt() {
#	rsync --exclude ".git/" \
#		--exclude ".DS_Store" \
#		--exclude "backup.sh" \
#		--exclude "brew.sh" \
#		--exclude "cask.sh" \
#		--exclude "git.sh" \
#		--exclude "lists/" \
#		--exclude "setup.sh" \
#		--exclude "sync.sh" \
#		--exclude "README.md" \
#		-avh --no-perms . ~;
#	source ~/.bash_profile;
#}

function cpdotfiles()
{
    for file in $(cat filelist)
	do
	    cp ${file} ~/.${file}
	done
}

if [ "$1" == "--force" -o "$1" == "-f" ]; then
	#doIt;
	cpdotfiles
else
	read -p "This may overwrite existing files in your home directory. Are you sure? (y/n) " -n 1;
	echo "";
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		#doIt;
		cpdotfiles
	fi;
fi;
unset doIt;
