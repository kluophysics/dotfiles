#!/usr/bin/env bash
brew doctor
#brew update

#brew bundle --file=./Brewfile

export DIR="$PWD/lists"
mkdir -p $DIR
#brew bundle dump > $DIR/brew.list
brew cask list > $DIR/cask.list
brew list > $DIR/brew.list 
pip freeze > $DIR/pip.list
