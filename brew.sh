#!/usr/bin/env bash

# install homebrew package manager
# /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# use caskroom as much as possible

brew install python python3
#brew install vim  #--with-lua 
brew install macvim
#brew install vim --override-system-vim

#brew tap homebrew/python
#brew install numpy scipy matplotlib ipython

#
#pip install ipython
#pip install virtualenvwrapper

brew install wget tree z gawk

# utilities tab completions
brew install bash-completion
#brew install homebrew/completions/brew-cask-completion #deprecated
brew install brew-cask-completion


# NodeJS
#brew install nodejs # or npm

# gnuplot needs aquaterm
brew install gnuplot --with-aquaterm --with-qt --with-x11

# install octave and
#brew install octave --with-gui

# install ctags
brew install ctags

# ruby on rails
# brew install rbenv ruby-build

# library for sci
brew install gcc
brew install gsl fftw
