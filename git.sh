#!/usr/bin/env bash

# setup for me
GIT_AUTHOR_NAME="Kai Luo"
GIT_COMMITTER_NAME="$GIT_AUTHOR_NAME"
git config --global user.name "$GIT_AUTHOR_NAME"
GIT_AUTHOR_EMAIL="kluophysics@gmail.com"
GIT_COMMITTER_EMAIL="$GIT_AUTHOR_EMAIL"
git config --global user.email "$GIT_AUTHOR_EMAIL"


# Tell Git to use osxkeychain helper using the global credential.helper config
#git credential-osxkeychain
git config --global credential.helper osxkeychain
